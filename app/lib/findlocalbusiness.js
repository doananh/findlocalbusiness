module.exports = {"localBusiness":[
  {
    "id": "1",
    "picture": "/images/test.jpg",
    "userCreated": {
      "first": "Vinh",
      "last": "An"
    },
    "Category": "Pharmacy",
    "bussinessName": "Vinh An Pharmacy",
    "email": "vinhan@gmail.com",
    "website": "vinhanpharmacy.vn",
    "phone": "(0511)2472699",
    "address": "313, Nguyen Hoang Q. Hai Chau",
    "dateCreated": "Monday, August 31, 2014 5:29 AM",
    "Description": "this is Description",
    "rating":"2",
    "latitude": "-82.461744",
    "longitude": "50.567202"
  },
  {
    "id": "2",
    "picture": "/images/test.jpg",
    "userCreated": {
      "first": "Hayden",
      "last": "Burgess"
    },
    "Category": "Cafes",
    "bussinessName": "Lela Cafes",
    "email": "hayden@burgess.net",
    "website": "lelacafe.vn",
    "phone": "01222 227 676",
    "address": "11,Pham Van Nghi Q.Thanh Khue",
    "dateCreated": "Sunday, May 24, 2015 2:10 PM",
    "Description": "this is Description",
    "rating":"1",
    "latitude": "80.483382",
    "longitude": "22.959608"
  },
  {
    "id": "3",
    "picture": "/images/test.jpg",
    "userCreated": {
      "first": "Camacho",
      "last": "Conway"
    },
    "Category": "Clothing",
    "bussinessName": "Winbi Clothing",
    "email": "camacho@conway.org",
    "website": "Winbi.io",
    "phone": "0908328191",
    "address": "311, Nguyen Hoang Q.Hai Chau",
    "dateCreated": "Thursday, May 22, 2014 1:58 AM",
    "Description": "this is Description",
    "rating":"4.5",
    "latitude": "16.382175",
    "longitude": "26.687768"
  },
  {
    "id": "4",
    "picture": "/images/test.jpg",
    "userCreated": {
      "first": "Christine",
      "last": "Frederick"
    },
    "Category": "Groceries",
    "bussinessName": "Bao Son Groceries",
    "email": "baoson@gmail.com",
    "website": "",
    "phone": "(0511)3655777",
    "address": "350, Nguyen Hoang Q.Thanh Khe",
    "dateCreated": "Monday, May 26, 2014 6:14 PM",
    "Description": "this is Description",
    "rating":"3",
    "latitude": "71.367779",
    "longitude": "-62.363554"
  },
  {
    "id": "5",
    "picture": "/images/test.jpg",
    "userCreated": {
      "first": "Lyons",
      "last": "Mccarty"
    },
    "Category": "Clothing",
    "bussinessName": "FM Style",
    "email": "SaleOnline@fmstyle.com.vn",
    "website": "Fmstyle.com.vn",
    "phone": "0997.56.999",
    "address": "415-417,Le Duan,Da Nang",
    "dateCreated": "Friday, February 28, 2014 1:30 AM",
    "Description": "this is Description",
    "rating":"3.5",
    "latitude": "43.148098",
    "longitude": "-100.601061"
  },
  {
    "id": "6",
    "picture": "/images/test.jpg",
    "userCreated": {
      "first": "Evans",
      "last": "Norton"
    },
    "Category": "Restaurants",
    "bussinessName": "Family Indian Restaurant",
    "email": "familyres.dn@gmail.com",
    "website": "http://www.indian-res.com/",
    "phone": "+84 511 3943 030",
    "address": "31 Ho Nghinh | Q.Son Tra, Ða Nang",
    "dateCreated": "Wednesday, October 28, 2015 2:04 AM",
    "Description": "this is Description",
    "rating":"5",
	"latitude": "-88.590895",
    "longitude": "-96.192655"
  },
  {
    "id": "7",
    "picture": "/images/test.jpg",
    "userCreated": {
      "first": "",
      "last": "Rodriguez"
    },
    "Category": "Cafes",
    "bussinessName": "Vu Cafes",
    "email": "macias@rodriguez.ca",
    "website": "vucafe.vn",
    "phone": "+1 (984) 412-2358",
    "address": "362, Nguyen Hoang Q.Thanh Khe",
    "dateCreated": "Saturday, January 25, 2014 11:26 AM",
    "Description": "this is Description",
    "rating":"5",
    "latitude": "-61.579326",
    "longitude": "12.85648"
  },
  {
    "id": "8",
    "picture": "/images/test.jpg",
    "userCreated": {
      "first": "Page",
      "last": "Wheeler"
    },
    "Category": "Hotels",
    "bussinessName": "SEA GARDEN Hotels",
    "email": "page@wheeler.net",
    "website": "pagemechanics.me",
    "phone": "+1 (850) 542-3190",
    "address": "29-33, Le Van Quy, An Hai Bac, Son Tra, Ða Nang",
    "dateCreated": "Saturday, May 10, 2014 4:57 PM",
    "Description": "this is Description",
    "latitude": "28.753571",
    "longitude": "-12.473163"
  },
  {
    "id": "9",
    "picture": "/images/test.jpg",
    "userCreated": {
      "first": "Duke",
      "last": "Farley"
    },
    "Category": "Motels",
    "bussinessName": "Duke Motels",
    "email": "duke@farley.org",
    "website": "dukemotels.io",
    "phone": "+1 (934) 400-3830",
    "address": "433 Midwood Street, Waverly, Arkansas, 7781",
    "dateCreated": "Monday, June 15, 2015 6:45 AM",
    "Description": "this is Description",
    "rating":"2.5",
    "latitude": "18.638307",
    "longitude": "-110.045443"
  },
  {
    "id": "10",
    "picture": "/images/test.jpg",
    "userCreated": {
      "first": "Shelly",
      "last": "Vazquez"
    },
    "Category": "Travel Agents",
    "bussinessName": "Shelly Travel Agents",
    "email": "shelly@vazquez.biz",
    "website": "shellytravel agents.biz",
    "phone": "+1 (813) 402-3378",
    "address": "233 Rost Place, Longoria, American Samoa, 7539",
    "dateCreated": "Tuesday, June 2, 2015 1:14 AM",
    "Description": "this is Description",
    "rating":"1",
    "latitude": "37.738039",
    "longitude": "158.698437"
  }
],
"catagory":[
{"id":"1","name":"Restaurants"},
{"id":"2","name":"Takeaways"},
{"id":"3","name":"Cafes"},
{"id":"4","name":"Pubs & Bars"},
{"id":"5","name":"Pharmacy"},
{"id":"6","name":"Plumbing"},
{"id":"7","name":"Electricians"},
{"id":"8","name":"Hotels"},
{"id":"9","name":"Motels"},
{"id":"10","name":"Clothing"},
{"id":"11","name":"Travel Agents"},
{"id":"12","name":"Car Rental"},
{"id":"13","name":"Groceries"},
{"id":"14","name":"Computers"},
{"id":"15","name":"Florists"},
{"id":"16","name":"Hairdressers"},
{"id":"17","name":"Plumbing"},
{"id":"18","name":"Day Spas"},
]
};
