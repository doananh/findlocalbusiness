var args = arguments[0] || {};
$.business.addEventListener('open', attach);
var business;
var catagory;
var selected = 0;
function attach(){
	var data = require('findlocalbusiness');
	catagory = data.catagory;
	business = data.localBusiness;
    initList();
    $.list.addEventListener('itemclick', itemClick);
    $.search.addEventListener('change',onchangeSearch);
    $.businessRating.init(doRating);
    $.list.searchText= "Restaurants";
    showlistChild(selected);
}

function doRating(e){
     alert('Your rating = ' + e);
}

function onchangeSearch(e){
    if(e.value != ""){
        $.list.searchText= e.value;
        aminationSearch(true);
    }
    else{
        aminationSearch(false);
    }
}

function aminationSearch(isshow){
    for (var i = 0; i < $.list.getSectionCount(); i++) {
        if(isshow)
            showlistChild(i);
        else
            hidelistChild(i);
    }
}
function initList(){
    //create searchtext for headerSection
    var searchString="*list";
	for (var i = 0; i < catagory.length; i++) {
        searchString+="|"+catagory[i].name;
    }
    //
	for (var i = 0; i < (catagory.length||0) ; i++) {
        var listObjects = [];
        var item = {
            headerIcon: {},
            catagory: {
                text: catagory[i].name || ' ',
            },
            icon: {
                text: '\uf3d1',
            },
            template:'templateParent',
            properties:{
                searchableText:searchString,
            }
        };
        listObjects.push(item);
        var templist = _.where(business, {Category:catagory[i].name || ' '}); 
        
        for(var j = 0; j< templist.length; j++){
        	var item = {
	            businessName: {
	            	text:templist[j].bussinessName|| " ",
	            },
	            businessRating:{children:[getRatingView(2).getView()]
                },
	            businessAddress: {
	                text:templist[j].address||" " ,
	            },
	            businessImage: {
                    image:templist[j].picture,
	            },
	            template:'templateChild',
	            properties:{
                searchableText :templist[j].bussinessName +"|"+ catagory[i].name
            }
        	};
        	// item.businessRating.init();
    		listObjects.push(item);
    	}
        var section = Ti.UI.createListSection();
        section.setItems(listObjects);

        $.list.insertSectionAt(i, section, {
            animated: false
        });
        listObjects = null;
        item = null;
    }
     searchString=null;
}
function getRatingView(stars){
  var rating = Alloy.createWidget('starrating', {
    height: 20,
    width: 10,
    max: 5,
    initialRating: 2
  });
  rating.init(doRating);
  return rating;
}
function showlistChild(e){
	 var isSelect = e;
	 var row = $.list.sections[isSelect].getItemAt(0);
    row.icon.text = "\uf3d0";
    $.list.sections[isSelect].updateItemAt(0, row, {
        animated: true
    });
    row = null;
}
function hidelistChild(e){
    var isSelect = e;
    var row = $.list.sections[isSelect||0].getItemAt(0);
    row.icon.text = "\uf3d1";
    $.list.sections[isSelect||0].updateItemAt(0, row, {
        animated: true
    });
    isSelect = null;
    row = null;
}

function clickItemBusiness(e){
    var detailBusinessController = Alloy.createController('detailBusiness',$.list.sections[isSelect||0].items[e.itemIndex]);
    $.business.close(); 
    detailBusinessController.getView().open();
    detailBusinessController =null;
    business = null;
    detach();
}
function itemClick(e){
    if(e.itemIndex == 0){
        if(e.sectionIndex != selected){
            $.list.searchText =$.list.sections[e.sectionIndex].items[0].catagory.text;
            hidelistChild(selected);
            selected = e.sectionIndex;
            showlistChild(selected);
        }
        else{
            $.list.searchText= "*list";
            hidelistChild(selected);
            selected = null;
        }
    }
}




function detach(){
	args = null;
	$.destroy();
	$.off();
}

exports.attach = attach;
exports.dettach = detach;