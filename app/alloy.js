// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
Alloy.Globals.FONTS = {
	ICON : "Ionicons",};
Alloy.Globals.BACKGROUNDS = {
	DARK : '#303F9F',
	DEFAULT:'#3F51B5',
	LIGHT:'#FFFFFF',
	ACCENT:'#607D8B',
	};	
Alloy.Globals.COLORS = {
	DEFAULT:'#FFFFFF',
	PRIMARY:'#212121' ,
	SECONDARY:'#727272',
	DIVIDER:'#B6B6B6',
	};